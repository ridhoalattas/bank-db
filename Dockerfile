FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

ENV DEBIAN_FRONTEND=noninteractive
ENV OMP_THREAD_LIMIT=1
ENV MODULE_NAME=server
ENV LOG_LEVEL=warning
ENV TIMEOUT=7200
ENV GRACEFUL_TIMEOUT=7200
RUN apt-get -y update && apt-get install -y apt-transport-https
RUN apt-get update -y; apt-get upgrade -y; apt-get install -y vim-tiny vim-athena ssh

ADD requirements.txt /app/

WORKDIR /app

RUN pip install --no-cache-dir --default-timeout=3600 -r requirements.txt

ADD . /app
