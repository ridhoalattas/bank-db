import psycopg2
import json
from psycopg2.extras import Json
from time import gmtime, strftime

from utils.db_helpers import random_no_rek

from fastapi import Form
from fastapi import APIRouter


conn = psycopg2.connect(
    database="db_bank",
    user="admin",
    password="admin",
    host="localhost"
)

cur = conn.cursor()

## Create Table
cur.execute('''CREATE TABLE IF NOT EXISTS BANK
      (ID            SERIAL PRIMARY KEY NOT NULL,
      NAMA           TEXT NOT NULL,
      NIK            TEXT NOT NULL,
      NO_HP          TEXT NOT NULL, 
      NO_REKENING    TEXT, 
      SALDO          INT, 
      MUTASI         JSONB);''')

router = APIRouter()

@router.post("/daftar")
async def daftar(
    nama: str = Form(...),  
    nik: str = Form(...),
    no_hp: str = Form(...)):

    if not isinstance(nama, str) or nama == None or nama =="":
        return "Wrong Input NAMA!"
    if not isinstance(nik, str) or nik == None or nik =="":
        return "Wrong Input NIK!"
    if not isinstance(no_hp, str) or no_hp == None or no_hp =="":
        return "Wrong Input NO_HP!"

    data_is_empty = False
    no_rek = random_no_rek(10)
    
    cur.execute("""SELECT * FROM BANK WHERE NIK='%s'""" %(nik,))
    rows = cur.fetchall()
    if len(rows) == 0 : 
        data_is_empty = True

        if data_is_empty : 
            cur.execute("INSERT INTO BANK(NAMA, NIK, NO_HP, NO_REKENING) VALUES (%s, %s, %s, %s)", \
                        (nama, nik, no_hp, no_rek))
            conn.commit()
            
            return {"nomor_rekening" : no_rek}
    
    else : 
        return "NIK sudah terdaftar !"
    
@router.post("/tabung")
async def tabung(
    no_rekening: str = Form(...),  
    nominal: int = Form(...)):

    if not isinstance(no_rekening, str) or no_rekening == None or no_rekening =="":
        return "Terjadi Kesalahan pada No_Rekening"
    if not isinstance(nominal, int) or nominal == None or nominal =="":
        return "Terjadi Kesalahan pada Nominal"
    
    cur.execute("""SELECT * FROM BANK WHERE NO_REKENING='%s'""" %(no_rekening,))
    rows = cur.fetchall()
    if len(rows) == 0 : 
        return "Nomor Rekening tidak Terdaftar"
    else : 
        for row in rows : 
            if row[5] is None : 
                saldo = 0
                mutasi = row[6]
            else : 
                saldo = row[5]
                mutasi = row[6]
        
        saldo_akhir = saldo + nominal

        kode_trans = random_no_rek(3)
        if mutasi == None : 
            mutasi = {'kode_transaksi' : kode_trans, 'nominal' : nominal, 'keterangan' :'TABUNG','waktu' : strftime("%Y-%m-%d %H:%M:%S", gmtime())}
            mutasi_new = json.dumps(mutasi)
        elif isinstance(mutasi, dict):
            mutasi_update = {'kode_transaksi' : kode_trans, 'nominal' : nominal, 'keterangan' :'TABUNG','waktu' : strftime("%Y-%m-%d %H:%M:%S", gmtime())}
            mutasi_new = json.dumps([mutasi, mutasi_update])
        elif isinstance(mutasi, list):
            mutasi_update = {'kode_transaksi' : kode_trans, 'nominal' : nominal, 'keterangan' :'TABUNG','waktu' : strftime("%Y-%m-%d %H:%M:%S", gmtime())}
            mutasi.append(mutasi_update)
            mutasi_new = json.dumps(mutasi)

        cur.execute("""UPDATE BANK SET SALDO = '%s', MUTASI = '%s'""" %(saldo_akhir, mutasi_new))
        conn.commit()

        return {"saldo" : saldo_akhir}

@router.post("/tarik")
async def tarik(
    no_rekening: str = Form(...),  
    nominal: int = Form(...)):

    if not isinstance(no_rekening, str) or no_rekening == None or no_rekening =="":
        return "Terjadi Kesalahan pada No_Rekening"
    if not isinstance(nominal, int) or nominal == None or nominal =="":
        return "Terjadi Kesalahan pada Nominal"
    
    cur.execute("""SELECT * FROM BANK WHERE NO_REKENING='%s'""" %(no_rekening,))
    rows = cur.fetchall()
    if len(rows) == 0 : 
        return "Nomor Rekening tidak Terdaftar"
    else : 
        for row in rows : 
            if row[5] is None : 
                saldo = 0
                mutasi = None
            else : 
                saldo = row[5]
                mutasi = row[6]
        
        saldo_akhir = saldo - nominal

        if saldo_akhir < 0 : 
            return "Nominal yang diinginkan tidak sesuai"
        else : 
            kode_trans = random_no_rek(3)
            if isinstance(mutasi, dict):
                mutasi_update = {'kode_transaksi' : kode_trans, 'nominal' : nominal, 'keterangan' :'TARIK','waktu' : strftime("%Y-%m-%d %H:%M:%S", gmtime())}
                mutasi_new = json.dumps([mutasi, mutasi_update])
            elif isinstance(mutasi, list):
                mutasi_update = {'kode_transaksi' : kode_trans, 'nominal' : nominal, 'keterangan' :'TARIK','waktu' : strftime("%Y-%m-%d %H:%M:%S", gmtime())}
                mutasi.append(mutasi_update)
                mutasi_new = json.dumps(mutasi)

            cur.execute("""UPDATE BANK SET SALDO = '%s', MUTASI = '%s'""" %(saldo_akhir,mutasi_new))
            conn.commit()

            return {"saldo" : saldo_akhir}
    
@router.get("/saldo/{no_rekening}")
async def saldo(no_rekening):

    if not isinstance(no_rekening, str) or no_rekening == None or no_rekening =="":
        return "Terjadi Kesalahan pada No_Rekening"
    
    cur.execute("""SELECT * FROM BANK WHERE NO_REKENING='%s'""" %(no_rekening,))
    rows = cur.fetchall()
    if len(rows) == 0 : 
        return "Nomor Rekening tidak Terdaftar"
    else : 
        for row in rows : 
            if row[5] is None : 
                saldo = 0
            else : 
                saldo = row[5]
        
        return {"saldo" : saldo}

@router.get("/mutasi/{no_rekening}")
async def mutasi(no_rekening):

    if not isinstance(no_rekening, str) or no_rekening == None or no_rekening =="":
        return "Terjadi Kesalahan pada No_Rekening"
    
    cur.execute("""SELECT * FROM BANK WHERE NO_REKENING='%s'""" %(no_rekening,))
    rows = cur.fetchall()
    if len(rows) == 0 : 
        return "Nomor Rekening tidak Terdaftar"
    else : 
        for row in rows : 
            if row[6] is None : 
                mutasi = None
            else : 
                mutasi = row[6]
        
        return {"mutasi" : mutasi}