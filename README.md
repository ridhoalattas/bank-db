### Achmad Ridho

Berikut adalah step by step menjalankan program : 
1. Jalankan docker terlebih dahulu "docker-compose up" menggunakan command prompt
2. Jalankan server.py dengan command prompt baru

Berikut port yang tersedia : 
1. http://localhost:6000/daftar Req : (nama, nik dan no_hp)
    -> Contoh Return 
    {
        "nomor_rekening": 7771037543
    }

2. http://localhost:6000/tabung Req : (no_rekening, nominal) 
    -> Contoh Return 
    {
        "saldo": 200000
    }

3. http://localhost:6000/tarik
    -> Contoh Return 
    {
        "saldo": 100000
    }

4. http://localhost:6000/saldo/7771037543
    -> Contoh Return 
    {
    "saldo": 100000
    }

5. http://localhost:6000/mutasi/7771037543
    -> Contoh Return 
    {
        "mutasi": [
            {
                "waktu": "2023-03-29 14:11:56",
                "nominal": 200000,
                "keterangan": "TABUNG",
                "kode_transaksi": 647
            },
            {
                "waktu": "2023-03-29 14:12:05",
                "nominal": 100000,
                "keterangan": "TARIK",
                "kode_transaksi": 177
            }
        ]
    }
